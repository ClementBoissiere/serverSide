package fr.oss.network;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {
	public static ServerSocket ss = null;
	public static Thread t;


	public void startServer() {
		
		try {
			ss = new ServerSocket(6405);
			System.out.println("The server listen on port : "+ss.getLocalPort());

			t = new Thread(new Connection_handler(ss));
			t.start();

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

	}


}
