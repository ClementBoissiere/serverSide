package fr.oss.boat;

import fr.oss.game.Constant;

public class Battleship extends Boat {

	private Integer size;
	private String shipChar = Constant.SHIP_BATTLESHIP_CHAR;
	
	public Battleship() {
		this.size = 5;
	}

	public Integer getSize() {
		return size;
	}
	
	public String getShipChar() {
		return shipChar;
	}
	
	public void setShipChar(String shipChar) {
		this.shipChar = shipChar;
	}
}