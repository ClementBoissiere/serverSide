package fr.oss.game;


import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;


public class HistoryBoard {

    private Socket socket = null;
    private PrintWriter out = null;
    private String[][] board = new String[10][10];

    public HistoryBoard(Socket s) {
        this.socket = s;
        this.initBuffers();
        setBoard();
    }

    private void initBuffers() {
        try {
            out = new PrintWriter(this.socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error on the socket !\n");
        }
    }

    public void talk(String msg) {
        out.println(msg);
    }

    public void fireWhenHit(int x, int y) {
		x = x - 1;
		y = y - 1;
        talk("HISTORY BOARD : ");
        if (x < 10 || x > 0 || y < 10 || y > 0) {
        	board[y][x] = "H";
        }
        printBoard();
    }
    
    public void fireWhenMiss(int x, int y) {
		x = x - 1;
		y = y - 1;
        talk("HISTORY BOARD : ");
        board[y][x] = "x";
        printBoard();
    }
    
    private void setBoard() {
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board[x].length; y++) {
                board[x][y] = "0";
            }
        }
    }

    public void printBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int c = 0; c < board[i].length; c++) {
                out.print(board[i][c] + " ");
            }
            talk(" ");
        }
    }
    
    public String[][] getBoard() {
        return board;
    }

    public void setBoard(String[][] board) {
        this.board = board;
    }
}