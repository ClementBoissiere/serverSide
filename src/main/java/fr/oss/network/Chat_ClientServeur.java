package fr.oss.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import fr.oss.game.Constant;
import fr.oss.game.Game;
import fr.oss.game.GameList;
import fr.oss.game.Player;
import fr.oss.game.WaitingList;

public class Chat_ClientServeur implements Runnable {

    private Socket socket = null;
    private BufferedReader in = null;
    private PrintWriter out = null;
    private Game game = null;
    private boolean isOver = false;
    private String clientIp;

    public Chat_ClientServeur(Socket s) {
        this.socket = s;
        this.clientIp = socket.getRemoteSocketAddress().toString();
        this.initBuffers();
        this.usage("");
    }

   public void talkToBothPlayers(String msg) {
	    game = GameList.getInstance().findGame(this.clientIp);
    	game.findPlayerByIp(this.clientIp).getGameBoard().talk(msg);
    	game.findOtherPlayerByIp(this.clientIp).getGameBoard().talk(msg);
    }
    private void initBuffers() {
        try {
            in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            out = new PrintWriter(this.socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error on the socket !\n");
        }
    }

    private String[] readCommand() {
        try {
            if (in.ready()) {
                return in.readLine().split(" ");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error on the command !\n");
        }
        return new String[]{};
    }

    public void talk(String msg) {
        out.println(msg);
    }

    private void actionCherche() {
    	game = GameList.getInstance().findGame(this.clientIp);
    	if (game != null) {
    		talk("You're already in game");
    	} else {
	        int addplayer = WaitingList.getInstance().addPlayer(this.clientIp, socket);
	        if (addplayer == 0) {
	            talkToBothPlayers("Game starting !");
	        } else if (addplayer == 1) {
	            talk("Waiting for players...");
	        } else {
	            talk("You're already searching for a game");
	        }
    	}
    }

    private boolean actionPlacer(String[] cmd) {
        if (cmd.length < 5) {
            return false;
        }
        game = GameList.getInstance().findGame(this.clientIp);
        int x = Integer.parseInt(cmd[1]);
        int y = Integer.parseInt(cmd[2]);
        String orientation = cmd[3];
        String type = cmd[4];
        game.findPlayerByIp(this.clientIp).getGameBoard().AddBoatOnBoard(x, y, orientation, type);
        return true;
    }

    private void actionQuit() {
        try {
            this.isOver = true;
            this.talk("Bye !");
            this.in.close();
            this.out.close();
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean actionFire(String[] cmd) {
        if (cmd.length < 3) {
            talk("Bad arguments !");
            return false;
        }
        game = GameList.getInstance().findGame(this.clientIp);
        if (!game.findOtherPlayerByIp(this.clientIp).isReady()) {
            talk("Your enemy isn't ready yet !");
            return false;
        }
        int x = Integer.parseInt(cmd[1]);
        int y = Integer.parseInt(cmd[2]);
        int fire = game.findOtherPlayerByIp(this.clientIp).getGameBoard().fire(x, y);
        if (fire == 0) {
            game.findPlayerByIp(this.clientIp).getHistoryBoard().fireWhenHit(x, y);
            talk("-------------------");
            talk("Gameboard");
            game.findPlayerByIp(this.clientIp).getGameBoard().printBoard();
        	talkToBothPlayers("Touched");
            run();
        } else if (fire == 1){
        	game.findPlayerByIp(this.clientIp).getHistoryBoard().fireWhenMiss(x, y);
            talk("-------------------");
            talk("Gameboard");
            game.findPlayerByIp(this.clientIp).getGameBoard().printBoard();
        	talkToBothPlayers("Rated");
            game.switchPlayer(game.findPlayerByIp(this.clientIp), game.findOtherPlayerByIp(this.clientIp));
        } else if (fire == 2){
            talk("-------------------");
            talk("Gameboard");
            game.findPlayerByIp(this.clientIp).getGameBoard().printBoard();
           	talkToBothPlayers("Couled");
            run();
        } else if (fire == 4){
			talk("Can't shoot here");
            run();
        } else if (fire == 5) {
			talk("You've already shooted here before !");
			run();
        } else {
            game.EndGame(game.findPlayerByIp(this.clientIp), game.findOtherPlayerByIp(this.clientIp));
        }
        return true;
    }

    private boolean actionReady() {
    	if(game.findPlayerByIp(this.clientIp).isReady()) {
    		talk("You are already ready");
    		return false;
    	}
        game.findPlayerByIp(this.clientIp).setReady(true);
        talk("You are ready");
        boolean readyToPlay = game.checkReady();
        if (readyToPlay) {
        	talkToBothPlayers("You are both ready, fight ! ");
        } else {
        	talk("You or your ennemy isn't ready yet !");
        }
        return true;
    }
    
    private boolean actionRandom() {
    	game = GameList.getInstance().findGame(this.clientIp);
    	if (game == null) {
    		talk("The game hasn't start yet ! ");
    		return false;
    	}  else {
        	game.findPlayerByIp(this.clientIp).getGameBoard().AddBoatOnBoardAleatoire();
        	return true;
    	}
    }
    
    private boolean actionKiller() {
    	game = GameList.getInstance().findGame(this.clientIp);
    	if (game == null) {
    		talk("The game hasn't start yet ! ");
    		return false;
    	}  else {
        	game.findPlayerByIp(this.clientIp).getGameBoard().killAllBoats();
        	game.godWin(game.findPlayerByIp(this.clientIp), game.findOtherPlayerByIp(this.clientIp));
        	return true;
    	}
    }

    private void usage(String action) {
        String usage = "";
        if ("".equals(action)) {
            usage += "Commandes disponibles : \n";
            usage += "\t 0 --> Search a game\n";
            usage += "\t 1 --> Place a ship\n";
            usage += "\t 2 --> Place ships randomly\n";
            usage += "\t 3 --> Fire\n";
            usage += "\t 4 --> Ready to play\n";
            usage += "\t 5 --> Quit\n";
        } else if (Constant.ACT_PLACE.equals(action)) {
            usage += "Placement settings : \n";
            usage += "\t x --> horizontal position on the board\n";
            usage += "\t y --> vertical position on the board\n";
            usage += "\t pos --> HORIZONTAL | VERTICAL\n";
            usage += "\t type --> [NAME_OF_SHIP_THIS_MOTHER]\n";
        } else if (Constant.ACT_FIRE.equals(action)) {
            usage += "Fire settings : \n";
            usage += "\t x --> horizontal position on the board\n";
            usage += "\t y --> vertical position on the board\n";
        }
        this.talk(usage);
    }

    public void run() {
        while (!isOver) {
            String[] cmd = this.readCommand();
            if (cmd.length > 0) {
                String action = cmd[0];
                if (Constant.ACT_QUIT.equals(action)) {
                    this.actionQuit();
                } else if (Constant.ACT_FIRE.equals(action)) {
                	if (game == null) {
                		this.talk("The game haven't start yet");
                	} else if (game.findPlayerByIp(this.clientIp).isBlocked()) { 
                        this.talk("It's not your turn !");
                    } else {
                        this.actionFire(cmd);
                    }
                } else if (Constant.ACT_SEARCH.equals(action)) {
                    this.actionCherche();
                } else if (Constant.ACT_PLACE.equals(action)) {
                	if (game == null) {
                		this.talk("The game haven't start yet");
                	} 
                    if (!this.actionPlacer(cmd)) {
                        usage(Constant.ACT_PLACE);
                    }
                } else if (Constant.ACT_READY.equals(action)) {
                	if (game == null) {
                		this.talk("The game haven't start yet");
                	} else {
                		this.actionReady();
                	}
                } else if (Constant.ACT_RANDOM.equals(action)) {
                	this.actionRandom();
                } else if (Constant.ACT_KILL.equals(action)) {
                	this.actionKiller();
                } else {
                    this.talk("Bad action : " + action);
                    this.usage("");
                }
            }
            try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
        System.out.println("Client " + clientIp + " just left !");
    }
}

