package fr.oss.boat;

public class BoatFactory {
	public Boat makeBoat(BoatType type) {
		Boat boat = null;
		switch(type) {
			case SUBMARINE:boat = new Submarine();break;
			case DESTROYER:boat = new Destroyer();break;
			case PATROLBOAT:boat = new Patrolboat();break;
			case CARRIER:boat = new Carrier();break;
			case BATTLESHIP:boat = new Battleship();break;
		}
		return boat;
	}

}
