package fr.oss.game;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import fr.oss.boat.Battleship;
import fr.oss.boat.Boat;
import fr.oss.boat.BoatType;
import fr.oss.boat.Carrier;
import fr.oss.boat.Destroyer;
import fr.oss.boat.Factory;
import fr.oss.boat.Patrolboat;
import fr.oss.boat.Submarine;
import fr.oss.network.Chat_ClientServeur;

public class GameBoard {

	private Socket socket = null;
	private PrintWriter out = null;
	private List<Boat> listBoat = null;
	private List<Boat> listBoat2 = null;
	private String[][] board = new String[10][10];
	private Factory factory = new Factory();
	private Boat battleship = null;
	private Boat carrier = null;
	private Boat submarine = null;
	private Boat cruiser = null;
	private Boat destroyer = null;
	private Boolean randomMode = false;

	public GameBoard(Socket s) {
		this.socket = s;
		this.initBuffers();
		this.listBoat = new ArrayList<>();
		this.listBoat2 = new ArrayList<>();
		this.battleship = factory.buildBoat(BoatType.BATTLESHIP);
		this.carrier = factory.buildBoat(BoatType.CARRIER);
		this.submarine = factory.buildBoat(BoatType.SUBMARINE);
		this.cruiser = factory.buildBoat(BoatType.PATROLBOAT);
		this.destroyer = factory.buildBoat(BoatType.DESTROYER);
		setBoard();
	}

	private void initBuffers() {
		try {
			out = new PrintWriter(this.socket.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error on the socket !\n");
		}
	}

	public void talk(String msg) {
		out.println(msg);
	}

	public boolean AddBoatOnBoard(int x, int y, String orientation, String boat) {
		boolean isInList = false;
		boolean placeSuccess = false;
		switch (boat) {
		case "BATTLESHIP":
			for (int i = 0; i < listBoat2.size(); i++) {
				if (listBoat2.get(i) instanceof Battleship) {
					if (!this.randomMode) {
						talk("You've already place this boat ! ");
					}
					isInList = true;
				}
			}
			if (canPlaceShip(x, y, this.battleship.getSize(), orientation) && !isInList) {
				battleship.setX(x);
				battleship.setY(y);
				if (orientation != "VERTICAL") {
					battleship.setVertical(false);
				}
				addBoat(x, y, orientation, battleship.getSize(), boat);
				listBoat.add(battleship);
				listBoat2.add(battleship);
				talk("Battleship added !");
				printBoard();
				placeSuccess = true;
			} 
			break;
		case "CARRIER":
			for (int i = 0; i < listBoat2.size(); i++) {
				if (listBoat2.get(i) instanceof Carrier) {
					if (!this.randomMode) {
						talk("You've already place this boat ! ");
					}
					isInList = true;
				}
			}
			if (canPlaceShip(x, y, this.carrier.getSize(), orientation) && !isInList) {
				carrier.setX(x);
				carrier.setY(y);
				if (orientation != "VERTICAL") {
					carrier.setVertical(false);
				}
				addBoat(x, y, orientation, 4, boat);
				listBoat.add(carrier);
				listBoat2.add(carrier);
				talk("Carrier added !");
				printBoard();
				placeSuccess = true;
			}
			break;
		case "DESTROYER":
			for (int i = 0; i < listBoat2.size(); i++) {
				if (listBoat2.get(i) instanceof Destroyer) {
					if (!this.randomMode) {
						talk("You've already place this boat ! ");
					}
					isInList = true;
				}
			}
			if (canPlaceShip(x, y, 3, orientation) && !isInList) {
				destroyer.setX(x);
				destroyer.setY(y);
				if (orientation != "VERTICAL") {
					destroyer.setVertical(false);
				}
				addBoat(x, y, orientation, 3, boat);
				listBoat.add(destroyer);
				listBoat2.add(destroyer);
				talk("Destroyer added !");
				printBoard();
				placeSuccess = true;
			}
			break;
		case "SUBMARINE":
			for (int i = 0; i < listBoat2.size(); i++) {
				if (listBoat2.get(i) instanceof Submarine) {
					if (!this.randomMode) {
						talk("You've already place this boat ! ");
					}
					isInList = true;
				}
			}
			if (canPlaceShip(x, y, 3, orientation) && !isInList) {
				submarine.setX(x);
				submarine.setY(y);
				if (orientation != "VERTICAL") {
					submarine.setVertical(false);
				}
				addBoat(x, y, orientation, 3, boat);
				listBoat.add(submarine);
				listBoat2.add(submarine);
				talk("Submarine added !");
				printBoard();
				placeSuccess = true;
			}
			break;
		case "PATROLBOAT":
			for (int i = 0; i < listBoat2.size(); i++) {
				if (listBoat2.get(i) instanceof Patrolboat) {
					if (!this.randomMode) {
						talk("You've already place this boat ! ");
					}
					isInList = true;
				}
			}
			if (canPlaceShip(x, y, 2, orientation) && !isInList) {
				cruiser.setX(x);
				cruiser.setY(y);
				if (orientation != "VERTICAL") {
					cruiser.setVertical(false);
				}
				addBoat(x, y, orientation, 2, boat);
				listBoat.add(cruiser);
				listBoat2.add(cruiser);
				talk("Patrolboat added !");
				printBoard();
				placeSuccess = true;
			}
			break;
		default:
			talk("Not a ship");
			break;
		}
		return placeSuccess;
	}

	public void addBoat(int x, int y, String orientation, int taille, String boat) {
		/* Replace indexes because the first index is [1,1] */
		x = x - 1;
		y = y - 1;
		if (orientation.equals("HORIZONTAL")) {
			for (int i = 0; i <= taille - 1; i++) {
				if (boat.equals("SUBMARINE")) {
					board[x + i][y] = Constant.SHIP_SUBMARINE_CHAR;
				} else if (boat.equals("BATTLESHIP")) {
					board[x + i][y] = Constant.SHIP_BATTLESHIP_CHAR;
				} else if (boat.equals("CARRIER")) {
					board[x + i][y] = Constant.SHIP_CARRIER_CHAR;
				} else if (boat.equals("DESTROYER")) {
					board[x + i][y] = Constant.SHIP_DESTROYER_CHAR;
				} else if (boat.equals("PATROLBOAT")) {
					board[x + i][y] = Constant.SHIP_PATROLBOAT_CHAR;
				} else {
					talk("Can't place the boat");
				}

			}
		} else if (orientation.equals("VERTICAL")) {
			for (int i = 0; i <= taille - 1; i++) {
				if (boat.equals("SUBMARINE")) {
					board[x][y + i] = Constant.SHIP_SUBMARINE_CHAR;
				} else if (boat.equals("BATTLESHIP")) {
					board[x][y + i] = Constant.SHIP_BATTLESHIP_CHAR;
				} else if (boat.equals("CARRIER")) {
					board[x][y + i] = Constant.SHIP_CARRIER_CHAR;
				} else if (boat.equals("DESTROYER")) {
					board[x][y + i] = Constant.SHIP_DESTROYER_CHAR;
				} else if (boat.equals("PATROLBOAT")) {
					board[x][y + i] = Constant.SHIP_PATROLBOAT_CHAR;
				} else {
					talk("Cant't place the boat");
				}
			}
		} else {
			talk("This orientation does not exist");
		}

	}

	public boolean canPlaceShip(int x, int y, int taille, String orientation) {
		/* Replace indexes because the first index is [1,1] */
		x = x - 1;
		y = y - 1;
		if (orientation.equals("VERTICAL") && (y + taille > 10 || y > 10 || y < 0)) {
			if (!this.randomMode) {
				talk("Can't place ship here");
			}
			return false;
		} else if (orientation.equals("HORIZONTAL") && (x + taille > 10 || x > 10 || x < 0)) {
			if (!this.randomMode) {
				talk("Can't place ship here");
			}
			return false;
		} else if (listBoat.size() >= 5) {
			talk("You've already place all your ships ! ");
			return false;
		}
		for (int i = 0 ; i < taille ; i++ ) {
		    if (orientation.equals("HORIZONTAL") && board[x + i][y] != "0") {
				if (!this.randomMode) {
					talk("A boat is already places at this place");
				}
				return false;
			} else if (orientation.equals("VERTICAL") && board[x][y + i] != "0"){
				if (!this.randomMode) {
					talk("A boat is already places at this place");
				}
				return false;
			}
		}
		return true;
	}

	public int fire(int x, int y) { 
        if (canFire(x, y) == 2) {
    		x = x - 1;
    		y = y - 1;
            if (board[x][y].equals("s")) {
                board[x][y] = "S";
                submarine.setNbShot(submarine.getNbShot() + 1);
                if(checkCouled(submarine)) {
                    listBoat.remove(submarine);
                    return 2;
                }
                if (checkNbBoat(listBoat)){
                    return 3;
                }
                printBoard();
                return 0;
            } else if (board[x][y].equals("b")) {
                board[x][y] = "B";
                battleship.setNbShot(battleship.getNbShot() + 1);
                if(checkCouled(battleship)) {
                    listBoat.remove(battleship);
                    return 2;
                }
                if(checkNbBoat(listBoat)){
                    return 3;
                }
                printBoard();
                return 0;
            } else if (board[x][y].equals("c")) {
                board[x][y] = "C";
                carrier.setNbShot(carrier.getNbShot() + 1);
                if(checkCouled(carrier)) {
                    listBoat.remove(carrier);
                    return 2;
                }
                if (checkNbBoat(listBoat)){
                    return 3;
                }
                printBoard();
                return 0;
            } else if (board[x][y].equals("p")) {
                board[x][y] = "P";
                cruiser.setNbShot(cruiser.getNbShot() + 1);
                if(checkCouled(cruiser)) {
                    listBoat.remove(cruiser);
                    return 2;
                }
                if (checkNbBoat(listBoat)){
                    return 3;
                }
                printBoard();
                return 0;
            } else if (board[x][y].equals("d")) {
                board[x][y] = "D";
                destroyer.setNbShot(destroyer.getNbShot() + 1);
                if(checkCouled(destroyer)) {
                    listBoat.remove(destroyer);
                    return 2;
                }
                if (checkNbBoat(listBoat)){
                    return 3;
                }
                printBoard();
                return 0;
            } else {
                board[x][y] = "x";
                printBoard();
                return 1;
            }
        } else if (canFire(x,y) == 0){
        	return 4;
        } else {
        	return 5;
        }
    }
	
	public boolean checkCouled(Boat boat){
        if (boat.getNbShot().equals(boat.getSize())) {
            boat.setState(true);
            return true;
        }else return false;
    }


    public boolean checkNbBoat(List<Boat> listBoat) {
        if (listBoat.size() == 0) {
            return true;
        }
        return false;
    }
    
	public int canFire(int x, int y) {
		x = x - 1;
		y = y - 1;
		if (x > 10 || x < 0 || y > 10 || y < 0) {
			return 0;
		} else if (board[x][y] == "x" || board[x][y] == "B" || board[x][y] == "S" || board[x][y] == "C"
				|| board[x][y] == "P" || board[x][y] == "D" || board[x][y] == "H") {
			return 1;
		} else {
			return 2;
		}
	}
	
	public void killAllBoats() {
		listBoat.clear();
	}

	private void setBoard() {
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board[x].length; y++) {
				board[x][y] = "0";
			}
		}
	}

	public boolean boatsAreAllPlaced() {
		if (listBoat2.size() == 5) {
			return true;
		}
		return false;
	}

	public void printBoard() {
		for (int y = 0; y < board.length; y++) {
			for (int x = 0; x < board[y].length; x++) {
				out.print(board[x][y] + " ");
			}
			talk(" ");
		}
	}

	public void AddBoatOnBoardAleatoire() {
		this.randomMode = true;
		Random rand = new Random();
		List<String> boats = Arrays.asList("BATTLESHIP", "DESTROYER", "CARRIER", "PATROLBOAT", "SUBMARINE");
		List<String> orientation = Arrays.asList("HORIZONTAL", "VERTICAL");
		for (int i = 0; i < boats.size(); i++) {
			boolean placeShipSuccess = false;
			int randomIndex = rand.nextInt(orientation.size());
			String randomOrient = orientation.get(randomIndex);
			if (listBoat2.size() != 5) {
				while(!placeShipSuccess) {
					int x = getRandomNumberInRange(1, 10);
					int y = getRandomNumberInRange(1, 10);
					placeShipSuccess = AddBoatOnBoard(x, y, randomOrient, boats.get(i));
				}
			} else {
				talk(boats.get(i) + " is already placed");
			}
		}

	}

	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		return (int) (Math.random() * ((max - min) + 1)) + min;
	}

	public String[][] getBoard() {
		return board;
	}

	public void setBoard(String[][] board) {
		this.board = board;
	}

	public List<Boat> getListBoat() {
		return listBoat;
	}

	public void setListBoat(List<Boat> listBoat) {
		this.listBoat = listBoat;
	}
}