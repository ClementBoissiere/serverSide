package fr.oss.game;

public class Constant {
	/* Commands */
    public static final String ACT_SEARCH = "0";
    public static final String ACT_PLACE = "1";
    public static final String ACT_RANDOM = "2";
    public static final String ACT_FIRE = "3";
    public static final String ACT_READY = "4";
    public static final String ACT_QUIT = "5";
    public static final String ACT_KILL = "BOMPARDESTUNDIEUVIVANT";
    
    /* Board */
    public static final String BOARD_WATER = "0";
    public static final String BOARD_MISSED = "x";
    public static final String BOARD_TOUCHED = "#";
    public static final int BOARD_HEIGHT = 10;
    public static final int BOARD_WIDTH = 10;
    
    /* Ships */
    public static final String SHIP_BATTLESHIP_CHAR = "b";
    public static final String SHIP_CARRIER_CHAR = "c";
    public static final String SHIP_DESTROYER_CHAR = "d";
    public static final String SHIP_PATROLBOAT_CHAR = "p";
    public static final String SHIP_SUBMARINE_CHAR = "s";
    
}
