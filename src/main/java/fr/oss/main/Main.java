package fr.oss.main;

import java.io.IOException;
import fr.oss.network.Server;



public class Main {

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.startServer();
    }
}
