package fr.oss.boat;

import fr.oss.game.Constant;

public class Carrier extends Boat {

	private Integer size;
	private String shipChar = Constant.SHIP_CARRIER_CHAR;

	public Carrier() {
		this.size = 4;
	}
	public Integer getSize() {

		return size;
	}

	public void setSize(Integer size) {

		this.size = size;
	}
	
	public String getShipChar() {
		return shipChar;
	}
}