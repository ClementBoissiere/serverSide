package fr.oss.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Connection_handler implements Runnable {

	private ServerSocket socketserver = null;
	private Socket socket = null;

	public Thread t1;

	public Connection_handler(ServerSocket ss) {
		socketserver = ss;
	}

	public void run() {
		try {
			while (true) {
				socket = socketserver.accept();
				System.out.println("A player has just connected  ");
				t1 = new Thread(new Chat_ClientServeur(socket));
				t1.start();
			}
		} catch (IOException e) {
			System.err.println("Server error");
		}
	}

}
