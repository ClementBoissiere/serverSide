package fr.oss.boat;

import fr.oss.game.Constant;

public class Patrolboat extends Boat {

	private Integer size;
	private String shipChar = Constant.SHIP_PATROLBOAT_CHAR;

	public Patrolboat() {
		this.size = 2;
	}
	public Integer getSize() {

		return size;
	}

	public void setSize(Integer size) {

		this.size = size;
	}
	
	public String getShipChar() {
		return shipChar;
	}
}