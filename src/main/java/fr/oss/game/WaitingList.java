package fr.oss.game;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class WaitingList {


	private static WaitingList INSTANCE = new WaitingList();

	private List<Player> waitingPlayerList = new ArrayList<>();

	public List<Player> getWaitingPlayerList() {
		return waitingPlayerList;
	}
	public void setWaitingPlayerList(List<Player> waitingPlayerList) {
		this.waitingPlayerList = waitingPlayerList;
	}

	public static WaitingList getInstance()
	{
		return INSTANCE;
	}

	public int addPlayer(String ip, Socket socket){
		Player player = new Player(ip, socket);
		for (Player aWaitingPlayerList : waitingPlayerList) {
			if (aWaitingPlayerList.getIp() == player.getIp()) {
				return 2;
			}
		}
		waitingPlayerList.add(player);
		if(waitingPlayerList.size() == 2){
			Game game = new Game(socket);
			game.startGame(waitingPlayerList.get(0), waitingPlayerList.get(1));
			waitingPlayerList.clear();
			return 0;
		}else{
			return 1;
		}
	}
	private WaitingList(){}


}