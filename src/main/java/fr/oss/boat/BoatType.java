package fr.oss.boat;

public enum BoatType {
	SUBMARINE,
	DESTROYER,
	PATROLBOAT,
	CARRIER,
	BATTLESHIP
}
