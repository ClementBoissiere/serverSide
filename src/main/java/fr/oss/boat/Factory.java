package fr.oss.boat;

public class Factory {
	private BoatFactory simpleFabrique;
	
	public Factory() {
		this.simpleFabrique = new BoatFactory();
	}
	
	public Boat buildBoat(BoatType type) {
		Boat boat = this.simpleFabrique.makeBoat(type);
		return boat;
	}
}
