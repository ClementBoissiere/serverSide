package fr.oss.game;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import fr.oss.network.Chat_ClientServeur;

public class Game {

    private Socket socket;
    private List<Player> listPlayer = new ArrayList<>();
    Chat_ClientServeur chat;

    public Game(Socket s) {
        socket = s;
        chat = new Chat_ClientServeur(socket);
    }

    public Game() {

    }

    public Player findPlayerByIp(String ip) {
        for (Player player : listPlayer) {
            if (player.getIp() == ip) {
                return player;
            }
        }
        return null;
    }

    public Player findOtherPlayerByIp(String ip) {
        for (Player player : listPlayer) {
            if (player.getIp() != ip) {
                return player;
            }
        }
        return null;
    }


    public void sendLog() {

    }

    public void EndGame(Player player1, Player player2) {
    	if(player1.getGameBoard().getListBoat().size() == 0) {
    		player1.getGameBoard().talk("Loosed");
    		player2.getGameBoard().talk("Winned");
    	} else {
    		player2.getGameBoard().talk("Loosed");
    		player1.getGameBoard().talk("Winned");
    	}
        player1.getGameBoard().talk("La partie est terminée");
        player2.getGameBoard().talk("La partie est terminée");
        player1.setBlocked(true);
        player2.setBlocked(true);
    }
    
    public void godWin(Player player1, Player player2) {
		player1.getGameBoard().talk("Winned");
		player2.getGameBoard().talk("The ennemy has used god mode, vous etes une merde");
    	player1.setBlocked(true);
        player1.getGameBoard().talk("La partie est terminée");
        player2.setBlocked(true);
        player2.getGameBoard().talk("La partie est terminée");
    }

	public void switchPlayer(Player currentPlayer, Player playerToswitch) {
        currentPlayer.setBlocked(true);
        currentPlayer.getGameBoard().talk("Ennemy's turn");
        playerToswitch.setBlocked(false);
        playerToswitch.getGameBoard().talk("You can play");
	}
	
	public boolean checkReady() {
		if (!listPlayer.get(0).getGameBoard().boatsAreAllPlaced() || !listPlayer.get(1).getGameBoard().boatsAreAllPlaced()) {
			return false;
		}
		if(listPlayer.get(1).isReady() && listPlayer.get(0).isReady()){
			switchPlayer(listPlayer.get(0), listPlayer.get(1));
			return true;
		}
		return false;
	}

    public void startGame(Player player1, Player player2) {
        player1.setColor("RED");
        getListPlayer().add(player1);
        player2.setColor("BLUE");
        getListPlayer().add(player2);
        GameList.getInstance().addGame(this);
    }

    public List<Player> getListPlayer() {
        return listPlayer;
    }

    public void setListPlayer(List<Player> listPlayer) {
        this.listPlayer = listPlayer;
    }
}