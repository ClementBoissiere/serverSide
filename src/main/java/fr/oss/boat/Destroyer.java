package fr.oss.boat;

import fr.oss.game.Constant;

public class Destroyer extends Boat {

	private Integer size;
	private String shipChar = Constant.SHIP_DESTROYER_CHAR;

	public Destroyer() {
		this.size = 3;
	}
	public Integer getSize() {

		return size;
	}

	public void setSize(Integer size) {

		this.size = size;
	}
	
	public String getShipChar() {
		return shipChar;
	}
}