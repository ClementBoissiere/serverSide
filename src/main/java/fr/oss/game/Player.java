package fr.oss.game;

import java.net.Socket;

public class Player extends Game {

	private Socket socket;
	private String color;
	private String ip;
	private GameBoard gameBoard;
	private boolean ready;
	private boolean blocked;
	private HistoryBoard historyBoard;

	public boolean isReady() {
		return ready;
	}
	public void setReady(boolean ready) {
		this.ready = ready;
	}
	public GameBoard getGameBoard() {
		return gameBoard;
	}
	
	public HistoryBoard getHistoryBoard() {
		return historyBoard;
	}
	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}
	public Player(String ip, Socket socket) {
		super();
		this.ip = ip;
		this.blocked = false;
		this.gameBoard = new GameBoard(socket);
		this.historyBoard = new HistoryBoard(socket);
	}
	public Socket getSocket() {
		return socket;
	}
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIp() { return ip; }

	public void setIp(String ip) { this.ip = ip; }
	public boolean isBlocked() {
		return blocked;
	}
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
}